;;;;A simple BYOND mode
;;To activate, load it in your .emacs and set it to automatically kick in with
;;(add-to-list 'auto-mode-alist '("\\.dm\\'" . byond-mode))


;;Could probably use better names?
(defvar byond-special '("if" "in" "var" "else" "switch" "break" "del" "return" "for" "while" "do" "continue" "new" "true" "false" "null" "const" "set" "to" "step" "global" "static" "arg" "as" "goto" "new" "sleep" "spawn" "tmp"))
(defvar byond-types '("obj" "mob" "turf" "proc" "world" "client" "savefile" "list" "verb" "atom" "datum" "area" "src" "icon" "image" "sound" "usr" "matrix"))
(defvar byond-vars '("address" "authenticate" "byond_version" "CGI" "ckey" "client" "command_text" "connection" "contents" "control_freak" "computer_id" "default_verb_category" "density" "desc" "dir" "edge_limit" "eye" "gender" "glide_size" "group" "icon" "icon_state" "images" "inactivity" "invisibility" "infra_luminosity" "key" "lazy_eye" "loc" "layer" "luminosity" "mouse_over_pointer" "mouse_drag_pointer" "mouse_drop_pointer" "mouse_drop_zone" "mouse_pointer_icon" "var" "mouse_opacity" "name" "opacity" "overlays" "override" "parent_type" "perspective" "pixel_x" "pixel_y" "pixel_z" "preload_rsc" "screen" "script" "see_infrared" "see_invisible" "see_in_dark" "show_map" "show_popup_menus" "show_verb_panel" "sight" "statobj" "statpanel" "suffix" "tag" "text" "type" "underlays" "vars" "verbs" "view" "virtual_eye" "x" "y" "z" "cache_lifespan" "category" "hidden" "popup_menu" "instant" "background" "alpha" "animate" "color" "blend_mode" "transform" "animate_movement" "bound_x" "bound_y" "bound_width" "bound_height" "locs" "screen_loc" "step_size" "step_x" "step_y"))
(defvar byond-procs '("Bump" "Click" "DblClick" "Del" "Enter" "Entered" "Exit" "Exited" "MouseDown" "MouseDrag" "MouseDrop" "MouseEntered" "MouseExited" "MouseUp" "Move" "New" "Read" "Stat" "Topic" "Write" "IconStates" "Turn" "Flip" "Shift" "SetIntensity" "Blend" "SwapColor" "DrawBox" "Insert" "MapColors" "Scale" "Crop" "GetPixel" "Width" "Height" "ClearMedal" "Export" "GetConfig" "GetMedal" "GetScores" "Import" "IsBanned" "OpenPort" "Reboot" "Repop" "SetConfig" "SetMedal" "SetScores" "Topic" "Cross" "Crossed" "Uncross" "Uncrossed" "rgb" "file" "browse" "rand" "prob"))
(defvar byond-compile-constants '("DM_VERSION" "__FILE__" "__LINE__" "__MAIN__" "FILE_DIR" "#define" "#if" "#undef" "#elif" "#ifdef" "#ifndef" "#else" "#endif" "#include" "#error" "#warn"))
(defvar byond-operators '("!" "~" "+" "-" "*" "/" "|" "&" "=" ">" "<" "%" "?" ":"))

(defvar byond-regex-special (regexp-opt byond-special 'symbols))
(defvar byond-regex-types (regexp-opt byond-types 'words))
(defvar byond-regex-vars (regexp-opt byond-vars 'symbols))
(defvar byond-regex-procs (regexp-opt byond-procs 'symbols))
(defvar byond-regex-constants (regexp-opt byond-compile-constants 'symbols))
(defvar byond-regex-operators (regexp-opt byond-operators))

(defvar byond-mode-tab-width 4) ;Set tab width in here

(defvar byond-mode-font-lock ;Order matters
  `(
    (,byond-regex-constants . font-lock-preprocessor-face)
    (,byond-regex-vars . font-lock-function-name-face)
    (,byond-regex-special . font-lock-keyword-face)
    (,byond-regex-types . font-lock-type-face)
    (,byond-regex-procs . font-lock-builtin-face)
    ("[0-9]" . font-lock-constant-face)
    (,byond-regex-operators . font-lock-builtin-face)
    )
  )

(defvar byond-mode-hook nil)
(defvar byond-mode-map
(let ((map (make-sparse-keymap)))
    (define-key map "\C-j" 'newline-and-indent)
	(define-key map (kbd "TAB") 'tab-to-tab-stop)
    map)
"Keymap for BYOND major mode")



(define-derived-mode byond-mode fundamental-mode "BYOND"
    "BYOND mode is a mode for editing DM files"
    (setq font-lock-defaults '((byond-mode-font-lock)))
    (when byond-mode-tab-width
        (setq tab-width byond-mode-tab-width))

    (modify-syntax-entry ?_ "." byond-mode-syntax-table)
    (modify-syntax-entry ?/ ". 124b" byond-mode-syntax-table)
    (modify-syntax-entry ?* ". 23" byond-mode-syntax-table)
    (modify-syntax-entry ?\n "> b" byond-mode-syntax-table)
    (modify-syntax-entry ?# "w" byond-mode-syntax-table)

)


;; Clear unneeded variables
(setq byond-special nil)
(setq byond-types nil)
(setq byond-vars nil)
(setq byond-procs nil)
(setq byond-compile-constants nil)
(setq byond-operators nil)

(setq byond-regex-special nil)
(setq byond-regex-types nil)
(setq byond-regex-vars nil)
(setq byond-regex-procs nil)
(setq byond-regex-constants nil)
(setq byond-regex-operators nil)

(provide 'byond-mode)
