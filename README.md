A simple mode that enable syntax highlighting for the BYOND language for emacs


##USAGE:
Load it at emacs startup and then either use `M-x byond-mode` in the dm file you want to edit or add `(add-to-list 'auto-mode-alist '("\\.dm\\'" . byond-mode))` to .emacs to automatically enable the mode when you edit a dm file
